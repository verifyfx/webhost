import React from 'react'
import Link from 'gatsby-link'

const Navbar = () => (
    <header>
        <ul id="headerButtons">
            <li className="navButton"><Link to="">Home</Link></li>
        </ul>
    </header>
)
  
  export default Navbar
  