module.exports = {
  siteMetadata: {
    title: 'MUICCollective',
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
