import get from 'lodash/get'
import React from 'react'
import ReactDOM from 'react-dom'
import Helmet from 'react-helmet'
import { siteMetadata } from '../../../gatsby-config'
import { db } from '../../firebase';
import { navigateTo } from 'gatsby-link';

class EnrollEvent extends React.Component {
    constructor() {
        super();

        this.formRegistration = {
            name: null,
            studentId: null,
            major: null,
            status: null,
            companyName: null,
            email: null,
            telno: null,
            reason: null,
            comment: null,
            timestamp: null
        }
        this.otherStatus = false;

        this.handleStatus = this.handleStatus.bind(this);
        this.handleJoinReason = this.handleJoinReason.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleStatus(event) {
        this.formRegistration.status = event.target.value;

        var inputNode = ReactDOM.findDOMNode(this.refs.studentStatusOther);
        var node = ReactDOM.findDOMNode(this.refs.frmStatusOther);

        if (event.target.value == "Other") {
            inputNode.value = "";
            node.classList.remove('frm-hidden');
        } else {
            node.classList.add('frm-hidden')
            inputNode.value = event.target.value;
        }
    }

    handleJoinReason(event) {
        this.formRegistration.status = event.target.value;

        var inputNode = ReactDOM.findDOMNode(this.refs.reasonOther);
        var node = ReactDOM.findDOMNode(this.refs.frmJoinReasonOther);

        if (event.target.value == "Other") {
            inputNode.value = "";
            node.classList.remove('frm-hidden');
        } else {
            node.classList.add('frm-hidden')
            inputNode.value = event.target.value;
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        this.formRegistration.name = data.get("name");
        this.formRegistration.studentId = data.get("student-id");
        this.formRegistration.major = data.get("student-major");
        this.formRegistration.status = data.get("student-status-other");
        this.formRegistration.companyName = data.get("name-of-workplace");
        this.formRegistration.email = data.get("email");
        this.formRegistration.telno = data.get("phone-number");
        this.formRegistration.reason = data.get("reason-other");
        this.formRegistration.comment = data.get("comment");
        this.formRegistration.timestamp = new Date().toUTCString()

        db.doRegisterEvent(this.formRegistration).then(function onSuccess(res) {
            navigateTo('/registration-thankyou');
        }).catch(function onError(err) {
            alert("An error has occoured during submitting your information. Please try again later.");
        });
    }

    render() {
        const pathPrefix =
            process.env.NODE_ENV === 'development' ? '' : __PATH_PREFIX__
        const title = 'Enroll to an event'
        return (<div>
            <Helmet title={`${title} | ${get(siteMetadata, 'title')}`} meta={[ { name: 'twitter:card', content: 'summary' }, { name: 'twitter:site', content: `@${get(siteMetadata, 'twitter')}`, }, { property: 'og:title', content: title }, { property: 'og:type', content: 'website' }, { property: 'og:description', content: get(siteMetadata, 'description'), }, { property: 'og:url', content: `${get(siteMetadata, 'siteUrl')}/EnrollEvent`, }, { property: 'og:image', content: `${get(siteMetadata, 'siteUrl')}/img/MUICCollective.png`, }, ]} />
            <section className="text">
                <div className="container-fluid">
                    <div className="row header-bg fluid">
                        <table className="header-bg-table">
                            <tbody>
                                <tr>
                                    <td className="text-center">
                                        <h2 className="event-text-sub">MUICCollective Presents</h2>
                                        <h1 className="event-text">100 วัน<br />หลัง<br />เรียนจบ</h1>
                                        <h3 className="event-text-body">ณ MUIC Co-working Space วันที่ 21 กรกฏาคม 2561 เวลา 13.00 - 17.00 น.</h3>
                                        {/* <a href="#registration-form" className="btn btn-primary event-text-body">ลงทะเบียนเข้าร่วมงาน</a> */}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="container-fluid event-text-body">
                    <a name="registration-form" />
                    <div className="row">
                        <div className="col-xs-12 col-md-6 col-lg-6 col-centered">
        
                            <div className="row">
                                <div className="col-lg-12">
                                    <h1 className="text-dark">Registration form</h1>
                                    <h2>100 วัน หลังเรียนจบ</h2>
                                    <br />
                                </div>
                            </div>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="Name">ชื่อ</label>
                                    <div className="input-group">
                                        <input id="name" name="name" placeholder="Your full name" required="required" className="form-control here" type="text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="student-id">รหัสนักศึกษา</label>
                                    <div className="input-group">
                                        <input id="student-id" name="student-id" placeholder="Student ID" className="form-control here" required="required" type="number" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="student-major">เมเจอร์</label>
                                    <div>
                                        <select id="student-major" name="student-major" defaultValue="none" className="custom-select" required="required">
                                            <option value="none" disabled>Please select...</option>
                                            <option value="Business Economics">Business Economics</option>
                                            <option value="Finance">Finance</option>
                                            <option value="International Business">International Business</option>
                                            <option value="Marketing">Marketing</option>
                                            <option value="International Hospitality Management">International Hospitality Management</option>
                                            <option value="Media and Communication">Media and Communication</option>
                                            <option value="Applied Mathematics">Applied Mathematics</option>
                                            <option value="Biological Sciences">Biological Sciences</option>
                                            <option value="Chemistry">Chemistry</option>
                                            <option value="Computer Science">Computer Science</option>
                                            <option value="Environmental Science">Environmental Science</option>
                                            <option value="Food Science and Technology">Food Science and Technology</option>
                                            <option value="Physics">Physics</option>
                                            <option value="Communication Design">Communication Design</option>
                                            <option value="Animation Production">Animation Production</option>
                                            <option value="Film Production">Film Production</option>
                                            <option value="Television Production">Television Production</option>
                                            <option value="Intercultural Studies and Languages">Intercultural Studies and Languages</option>
                                            <option value="Social Science">Social Science</option>
                                            <option value="Computer Engineering">Computer Engineering</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="student-status">สถานะ</label>
                                    <div>
                                        <select id="student-status" name="student-status" defaultValue="none" required="required" className="custom-select" onChange={this.handleStatus}>
                                            <option value="none" disabled>Please select...</option>
                                            <option value="พนักงานเงินเดือน">พนักงานเงินเดือน</option>
                                            <option value="สานต่อธุรกิจที่บ้าน">สานต่อธุรกิจที่บ้าน</option>
                                            <option value="เริ่มทำ Start Up">เริ่มทำ Start Up</option>
                                            <option value="เรียนต่อ">เรียนต่อ</option>
                                            <option value="ฟรีแลนซ์">ฟรีแลนซ์</option>
                                            <option value="ไม่ได้ทำอะไร">ไม่ได้ทำอะไร</option>
                                            <option value="Other">อื่นๆ</option>
                                        </select>
                                    </div>
                                </div>
                                <div ref="frmStatusOther" id="frm-status-other" name="frm-status-other" className="form-group frm-hidden">
                                    <label htmlFor="student-status-other">สถานะของคุณ</label>
                                    <div className="input-group">
                                        <input ref="studentStatusOther" id="student-status-other" name="student-status-other" placeholder="Your status" required="required" className="form-control here" type="text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name-of-workplace">ชื่อบริษัท / ชื่อองค์กร / ชื่อสถานศึกษา</label>
                                    <div className="input-group">
                                        <input id="name-of-workplace" name="name-of-workplace" placeholder="Name of related company / organization / academic place" className="form-control here" type="text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email Address ของคุณ ที่เราสามารถติดต่อได้</label>
                                    <div className="input-group">
                                        <input id="email" name="email" placeholder="Email address" className="form-control here" required="required" aria-describedby="emailHelpBlock" type="email" />
                                    </div>
                                    <span id="emailHelpBlock" className="form-text text-muted">Need to be valid email, we will send out the ticket there.</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone-number">เบอร์โทรติดต่อ</label>
                                    <div className="input-group">
                                        <input id="phone-number" name="phone-number" placeholder="Phone number" required="required" className="form-control here" type="tel" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="reason">เหตุผลที่มาเข้าร่วม</label>
                                    <div>
                                        <select id="reason" name="reason" className="custom-select" defaultValue="none" required="required" onChange={this.handleJoinReason}>
                                            <option value="none" disabled>Please select...</option>
                                            <option value="ค้นหาตัวเอง / อยากมีเป้าหมายในชีวิต">ค้นหาตัวเอง / อยากมีเป้าหมายในชีวิต</option>
                                            <option value="เพื่อหางาน / โอกาส">เพื่อหางาน / โอกาส</option>
                                            <option value="ไม่ชอบงานที่ทำอยู่ แต่ไม่รู้ว่าจะทำอะไรดี">ไม่ชอบงานที่ทำอยู่ แต่ไม่รู้ว่าจะทำอะไรดี</option>
                                            <option value="กำลังเรียนจบ อยากมาฟังไอเดีย">กำลังเรียนจบ อยากมาฟังไอเดีย</option>
                                            <option value="อยากรู้จักคนอื่นๆ อยากสร้าง connection">อยากรู้จักคนอื่นๆ อยากสร้าง connection</option>
                                            <option value="Other">อื่นๆ</option>
                                        </select>
                                    </div>
                                </div>
                                <div ref="frmJoinReasonOther" id="frm-join-reason-other" name="frm-join-reason-other" className="form-group frm-hidden">
                                    <label htmlFor="reason-other">เหตุผลของคุณ</label>
                                    <div className="input-group">
                                        <input ref="reasonOther" id="reason-other" name="reason-other" placeholder="Your reason" required="required" className="form-control here" type="text" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="comment">ความคิดเห็นเพิ่มเติม</label>
                                    <textarea id="comment" name="comment" cols="40" rows="5" className="form-control"></textarea>
                                </div>
                                <div className="form-group">
                                    <button name="submit" type="submit" className="btn btn-primary">Submit Registration Form</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="container text-justify event-text-body">
                    <br />
                    <h1 className="text-dark">“ยังไงต่อ” </h1>
                    <br />
                    <p>คำ 3 พยางค์ ที่เรามักพูดเสมอตอนที่เราเจอทางตันหรือ คิดไม่ออกในขณะนั้นว่าจะทำยังไงต่อ
                        <br /> ตัวอย่างเช่น:
                    </p>
                    <ul>
                        <li>คนในกลุ่มไม่ช่วยทำ Project เราจะทำ <strong>“ยังไงต่อ”</strong></li>
                        <li>เหล้าหมดแล้ว ก็จะมีเพื่อนพูดว่า <strong>“ยังไงต่อ”</strong></li>
                        <li>ไม่รู้จะยกตัวอย่างอะไรให้อ่านในประโยคนี้ คนเขียนก็จะคิดว่า <strong>“ยังไงต่อ”</strong></li>
                    </ul>
                    <p>
                        คำๆ นี้เกิดขึ้นบ่อย และได้ยินเสมอเพราะว่ามันถูกจับคู่กับสถานการณ์ที่ไม่ค่อยเอื้ออำนวยให้เรานัก แต่สำหรับผมคำ 3 พยางค์นี้ กลายเป็นคำที่มีน้ำหนักที่ทำให้ผมคิดมาก มากกว่าตอนที่แม่พูดว่า “อย่าให้แม่นับถึง 3 นะ”
                    </p>
                    <p>
                        สิ่งที่เกิดขึ้นคือ เราได้เอาคำว่า “ยังไงต่อ” มาต่อท้ายกับคำว่า “เรียนจบ” และ ณ ช่วงเวลาที่เราคิดนั้น มันคือจุดจบของการใช้ชีวิตมหาลัย แต่ มันได้กลายมาเป็นจุดเริ่มต้นของ Project นี้
                    </p>
                    <p>
                        “เรียนจบแล้ว...ยังไงต่อ” คือประโยคที่เรา MUIC Collective เห็นว่ามันไม่ได้เป็นแค่คำ 3 พยางค์ แต่มันคือคำ 5 พยางค์ ที่มี IMPACT มากกว่าเดิมเพราะ การที่เราไม่ต้องไปห้องเรียนตามเวลา และออกมาจากเขตศาลายานั้น ทำให้เราเห็นอะไรมากขึ้น เช่นเส้นทางชีวิต หรือที่เราขอเรียกว่า “รูปแบบสาย” ที่แต่ละคนจะเลือกหลังจากเรียนจบ
                    </p>
        
                    <p>
                        <strong>“รูปแบบสาย”</strong> ที่เราหมายถึงนั้นคือ
                    </p>
                    <ul>
                        <li>Persuring Higher Degree</li>
                        <li>Corporate Employment</li>
                        <li>Home Business</li>
                        <li>Startup</li>
                        <li>Freelance</li>
                        <li>Gap Year</li>
                    </ul>
                    <p>
                        ด้วยความรู้และความเข้าใจของแต่ละสายที่จำกัด เนื่องจากขาดเนื้อหาและความเข้าใจจาก คนที่มีประสบการณ์จริงและเล่าความจริงนั้น ทำให้เราเห็นถึงความจำเป็นในการจัดงาน “100 วัน หลัง เรียน จบ”
                    </p>
                    <p>
                        ซึ่งเราอยากให้ทุกคนที่เรียนอยู่ นึกถึงคำว่า “เรียนจบแล้ว” และเพิ่มอีก 3 คำว่า “ยังไงต่อ” ลองถามตัวเองว่าเราจะอยู่ในสายไหน และแน่นอนก่อนที่จะเลือกอะไร ควรเข้าใจและศึกษาก่อน และงานนี้จะเป็นหนึ่งในคำตอบให้คุณ
                    </p>
                    <p>
                        ส่วนใครที่เรียนจบแล้ว งานนี้จะทำให้คุณเห็นถึง เส้นทางสายอื่นๆ ว่าจริงๆ แล้วเป็นยังไง คุณอาจจะเปลี่ยนความคิด เปลี่ยนใจว่าชอบหรือไม่ชอบอะไร หรือแม้กระทั่งปรับความเข้าใจใหม่ทั้งหมดเกี่ยวกับแต่ละสาย
                    </p>
                    <p className="lead text-muted">MUICCollective Team</p>
                    <br />
                    {/* <h1>ลงทะเบียนเข้าร่วมงานได้ข้างล่างนี้... แล้วเจอกัน</h1> */}
                </div>
            </section>
        </div>)
    }
}

export default EnrollEvent
