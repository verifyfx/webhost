---
title: "Take our survey"
date: "2018-06-16T22:12:03.284Z"
layout: post
path: "/slido-servey/"
categories:
  - Survey
---

Hey MUIC people ! If there is no one to answer your questions or if you wonder whether everyone has the same questions like you do, we are here to help you ! Post your questions or votes for interesting questions on Slido.com. #MUICCollective2018 will be active until next Sat 16th Jun
Noted: these questions will be answered in our first MUIC Collective's event. Details will be announced soon, please keep in touch !

<img height="310" src="./image.png" alt="Graduation">
<!--more-->

สวัสดีชาว MUIC ! ถ้าเพื่อนๆรู้สึกว่าไม่มีใครสามารถตอบคำถามที่ตัวเองมีได้ หรือสงสัยว่าคนอื่นๆมีคำถามหรือเจอปัญหาแบบเดียวกันไหม พวกเรา MUIC Collective สามารถช่วยได้ ! ขอเชิญเพื่อนๆโพสต์คำถามหรือโหวตให้กับคำถามที่น่าสนใจได้ใน Slido.com การโพสต์คำถามสามารถทำได้ตั้งแต่วันนี้จนถึงวันเสาร์หน้าที่ 16 มิถุนา
หมายเหตุ: คำถามจะถูกตอบในงานอีเว้นท์ครั้งแรกของเรา ข้อมูลเพิ่มเติมกำลังจะตามมา รอติดตามกันด้วย !
เพราะชีวิตมหาลัยอย่าง MUIC ไม่ควรจบแค่ในวันรับปริญญา แต่ประสบการณ์ในมหาวิทยาลัยควรจะเป็นสิ่งที่มีคุณค่าและเป็นส่วนหนึ่งของเราไปตลอดชีวิต

([Facebook Page](https://www.facebook.com/MUIC-Collective-175251059816513))