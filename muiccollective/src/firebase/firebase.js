import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const prodConfig = {
  apiKey: "AIzaSyDbCGD4N2iefJQCeJVgZWCfwEq__VWdAfY",
  authDomain: "webhost-171210.firebaseapp.com",
  databaseURL: "https://webhost-171210.firebaseio.com",
  projectId: "webhost-171210",
  storageBucket: "webhost-171210.appspot.com",
  messagingSenderId: "627235579597",
};

const devConfig = {
    apiKey: "AIzaSyDbCGD4N2iefJQCeJVgZWCfwEq__VWdAfY",
    authDomain: "webhost-171210.firebaseapp.com",
    databaseURL: "https://webhost-171210.firebaseio.com",
    projectId: "webhost-171210",
    storageBucket: "webhost-171210.appspot.com",
    messagingSenderId: "627235579597",
  };

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export {
  db,
  auth,
};